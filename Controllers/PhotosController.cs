using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using DatingApp.API.Data;
using DatingApp.API.Dtos;
using DatingApp.API.Helpers;
using DatingApp.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace DatingApp.API.Controllers
{
  [Authorize]
  [Route("api/users/{userId}/photos")]
  public class PhotosController : Controller
  {
    private readonly IDatingRepository _repo;
    private readonly IMapper _mapper;
    private readonly IOptions<CloudinarySettings> _cloudinaryConfig;
    private Cloudinary _cloudinary;

    public PhotosController(IDatingRepository repo,
    IMapper mapper,
    IOptions<CloudinarySettings> cloudinaryConfig)
    {
      _cloudinaryConfig = cloudinaryConfig;
      _mapper = mapper;
      _repo = repo;

      Account acc = new Account(
          _cloudinaryConfig.Value.CloudName,
          _cloudinaryConfig.Value.ApiKey,
          _cloudinaryConfig.Value.ApiSecret
      );

      // this particular object is gonna give access to upload photos to my account
      _cloudinary = new Cloudinary(acc);
    }

    [HttpGet("{id}", Name = "GetPhoto")]
    public async Task<IActionResult> GetPhoto(int id)
    {
      var photoFromRepo = await _repo.GetPhoto(id);

      var photo = _mapper.Map<PhotoForReturnDto>(photoFromRepo);

      return Ok(photo);
    }

    [HttpPost]
    public async Task<IActionResult> AddPhotoForUser(int userId, PhotoForCreationDto photoDto)
    {
      // check if it's the actual user
      var user = await _repo.GetUser(userId);

      if (user == null)
        return BadRequest("Could not find user");

      var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

      if (currentUserId != user.Id)
        return Unauthorized();

      // Upload the photo to cloudinary
      var file = photoDto.File;

      var uploadResult = new ImageUploadResult();

      if (file.Length > 0)
      {
        using (var stream = file.OpenReadStream())
        {
          var uploadParams = new ImageUploadParams()
          {
            File = new FileDescription(file.Name, stream),
            // the transformation param is added because we want the photos to be squared perfectly
            // so in the front end it shows nicely
            // we can do this so easily with cloudinay and cloudinary is smart enough to crop the photo around a face
            Transformation = new Transformation().Width(500).Height(500).Crop("fill").Gravity("face")
          };

          uploadResult = _cloudinary.Upload(uploadParams);
        }
      }

      // If everything goes well then get the url's photo to storage on the database
      photoDto.Url = uploadResult.Uri.ToString();
      photoDto.PublicId = uploadResult.PublicId;

      var photo = _mapper.Map<Photo>(photoDto);
      photo.User = user;

      if (!user.Photos.Any(m => m.IsMain))
        photo.IsMain = true;

      user.Photos.Add(photo);

      if (await _repo.SaveAll())
      {
        var photoToReturn = _mapper.Map<PhotoForReturnDto>(photo);
        return CreatedAtRoute("GetPhoto", new { id = photo.Id }, photoToReturn);
      }

      return BadRequest("Could not add the photo");
    }

    [HttpPost("{id}/setMain")]
    public async Task<IActionResult> SetMainPhoto(int userId, int id)
    {
      if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
        return Unauthorized();

      var photoFromRepo = await _repo.GetPhoto(id);
      if (photoFromRepo == null)
        return NotFound();

      if (photoFromRepo.IsMain)
        return BadRequest("This is already the main photo");

      var currentMainPhoto = await _repo.GetMainPhotoForUser(userId);
      if (currentMainPhoto != null)
        currentMainPhoto.IsMain = false;

      photoFromRepo.IsMain = true;

      if (await _repo.SaveAll())
        return NoContent();

      return BadRequest("Could not set photo to main");
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeletePhoto(int userId, int id)
    {
      if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
        return Unauthorized();

      var photoFromRepo = await _repo.GetPhoto(id);
      if (photoFromRepo == null)
        return NotFound();

      if (photoFromRepo.IsMain)
        return BadRequest("You cannot delete the main photo");

      if (photoFromRepo.PublicId != null)
      {
        var deleteParams = new DeletionParams(photoFromRepo.PublicId);

        var results = _cloudinary.Destroy(deleteParams);

        if (results.Result == "ok")
          _repo.Delete(photoFromRepo);
      }

      if (photoFromRepo.PublicId == null)
      {
        _repo.Delete(photoFromRepo);
      }

      if (await _repo.SaveAll())
        return Ok();

      return BadRequest("Failed to delete the photo");


    }
  }
}