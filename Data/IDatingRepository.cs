using System.Collections.Generic;
using System.Threading.Tasks;
using DatingApp.API.Helpers;
using DatingApp.API.Models;

namespace DatingApp.API.Data
{
    public interface IDatingRepository
    {
        void Add<T>(T entity) where T: class;
        void Delete<T>(T entity) where T: class;
        Task<bool> SaveAll();
        Task<PagedList<User>> GetUsers(UserParams userParams);
        Task<User> GetUser(int id);
        Task<Photo> GetPhoto(int id);
        Task<Photo> GetMainPhotoForUser(int userId);
        Task<Like> GetLike(int userId, int recipientId);
        Task<Message> GetMessage(int id);// this takes just one message by its ID
        Task<PagedList<Message>> GetMessagesForUser(MessageParams messageParams);// inbox and outbox for a user (it's kindda an unread messages scenario)
        Task<IEnumerable<Message>> GetMessageThread(int userId, int recipientId);// this will return a conversation between two users 
        // it's also called a message thread 
    }
}